# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'jenkins/version'

Gem::Specification.new do |spec|
  spec.name          = "jenkins-api"
  spec.version       = Jenkins::VERSION
  spec.authors       = ["Pavel Tsurbeleu"]
  spec.email         = ["pavel.tsurbeleu@me.com"]

  spec.summary       = %q{A simple client library for the Jenkins API}
  spec.description   = %q{A simple client library for the Jenkins API}
  spec.homepage      = "https://bitbucket.org/ptsurbeleu/jerkins-api.git"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "excon", "~> 0.55.0"
  spec.add_dependency "ox", "~> 2.4"

  spec.add_development_dependency "bundler", "~> 1.13"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "minitest", "~> 5.0"
  spec.add_development_dependency "pry", "~> 0.10.4"
  spec.add_development_dependency "pry-byebug", "~> 3.4"
  spec.add_development_dependency "pry-doc", "~> 0.10.0"
  spec.add_development_dependency "pry-rescue", "~> 1.4"
  spec.add_development_dependency "pry-stack_explorer", "~> 0.4.9.2"
end
