require 'test_helper'

class Jenkins::AllFilesInZipTest < Minitest::Test
  def setup
    folder = Jenkins::Folder.get('jenkins-api')
    @job = folder.get_job('all-files-in-zip')
  end

  test '#all_files_in_zip just works' do
    assert @job.all_files_in_zip
  end
end