require 'test_helper'

class Jenkins::FolderTest < Minitest::Test
  def setup
    # Folder name is Epoch timestamp
    @job_name = Time.now.to_i.to_s
  end

  def teardown
    Jenkins::Folder.delete(@job_name) if @job
  end

  test '#create creates a new folder' do
    Jenkins::Folder.create(@job_name)
    refute_nil @job = Jenkins::Folder.get(@job_name)
  end

  test '.delete removes the specified folder' do
    Jenkins::Folder.create(@job_name)
    Jenkins::Folder.delete(@job_name)
  end

  test '.get returns the specified folder' do
    # validate prerequisites
    assert_nil Jenkins::Folder.get(@job_name)
    # create folder
    Jenkins::Folder.create(@job_name)
    # get folder just created
    @job = Jenkins::Folder.get(@job_name)
    refute_nil @job
    assert_equal @job_name, @job.name
  end

  test '.all returns all folders' do
    refute_nil Jenkins::Folder.all
  end
end