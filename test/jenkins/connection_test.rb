require 'test_helper'

class Jenkins::ConnectionTest < Minitest::Test
  def setup
    @conn = Jenkins::Connection.new('https://jenkins.staticpages.io')
  end

  test '#initialize sets url' do
    assert_equal 'https://jenkins.staticpages.io', @conn.url
  end

  test '#get with no parameters (server_info)' do
    response = @conn.get('')
    refute_nil response
    assert response.start_with?('{"_class":"hudson.model.Hudson')
  end

  test '#crumb fetches security crumb' do
    response = @conn.crumb
    refute_nil response
    refute_nil response['Jenkins-Crumb']
  end
end