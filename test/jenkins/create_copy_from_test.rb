require 'test_helper'

class Jenkins::CreateCopyFromTest < Minitest::Test
  def setup
    # Folder name is Epoch timestamp
    @folder_name = Time.now.to_i.to_s
    Jenkins::Folder.create(@folder_name)
  end

  def teardown
    Jenkins::Folder.delete(@folder_name)
  end

  test '#create_copy_from' do
    @job_name = Time.now.to_i.to_s
    @folder = Jenkins::Folder.get(@folder_name)
    assert @folder.create_copy_from(@job_name, 'gallery/Lanyon')
  end
end