require 'test_helper'

class Jenkins::SetPropertiesTest < Minitest::Test
  def setup
    @folder, @job_name = Jenkins::Folder.get('noop'), Time.now.to_i.to_s
    @folder.create_copy_from(@job_name, 'gallery/Lanyon')
    @path = "/job/#{@folder.name}/job/#{@job_name}"
    @job = @folder.get_job(@job_name)
    @job_config = Jenkins::JobConfiguration.get(@path)
  end

  def teardown
    Jenkins::Job.delete(@path)
  end

  test '#set_properties of the specified job' do
    # ensure we are in a good shape
    param = @job_config.get_param('BRANCH_SPECIFIER')
    assert_equal '3.4.0', param.value
    # set properties
    @job.set_properties({ 'BRANCH_SPECIFIER' => '3.4.1' })
    # get job config again
    @job_config = @job.get_config
    # assert outcomes
    param = @job_config.get_param('BRANCH_SPECIFIER')
    assert_equal '3.4.1', param.value
  end
end