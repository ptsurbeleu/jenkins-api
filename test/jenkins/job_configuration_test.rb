require 'test_helper'

class Jenkins::JobConfigurationTest < Minitest::Test
  def setup
    @folder, @job_name = Jenkins::Folder.get('jenkins-api'), Time.now.to_i.to_s
    @folder.create_copy_from(@job_name, 'gallery/Lanyon')
    @path = "/job/jenkins-api/job/#{@job_name}"
    @config = Jenkins::JobConfiguration.get(@path)
  end

  def teardown
    Jenkins::Job.delete(@path)
  end

  test '.get returns specified job configuration' do
    refute_nil @config
  end

  test '#display_name returns display name' do
    @config = Jenkins::JobConfiguration.get("/job/jenkins-api/job/job-display-name")
    assert_equal 'my custom display name', @config.display_name
  end

  test '#display_name can set display name to be nil' do
    @config = Jenkins::JobConfiguration.get("/job/jenkins-api/job/job-display-name")
    @config.display_name = nil
    assert_nil @config.display_name
  end

  test '#display_name can set display name' do
    @config = Jenkins::JobConfiguration.get("/job/jenkins-api/job/job-display-name")
    @config.display_name = 'xoxoxo'
    assert_equal 'xoxoxo', @config.display_name
  end

  test '#quiet_period can get quiet period' do
    @config = Jenkins::JobConfiguration.get("/job/jenkins-api/job/quiet-period-120")
    assert_equal '120', @config.quiet_period
  end

  test '#quiet_period gets nil if not defined' do
    @config = Jenkins::JobConfiguration.get("/job/jenkins-api/job/no-quiet-period")
    assert_nil @config.quiet_period
  end

  test '.get can parse scm and remotes' do
    refute_nil @config.scm
    refute_nil @config.scm.remotes
  end

  test '.get can parse scm and branches' do
    refute_nil @config.scm
    refute_nil @config.scm.branches
  end

  test '.get can parse builders and script steps' do
    refute_nil @config.builders
    refute_nil @config.builders.scripts
  end

  test '.get can parse triggers' do
    refute_nil @config.triggers
  end

  test '.get can parse remote details' do
    remote = @config.scm.remotes.first
    assert_equal 'https://ptsurbeleu@bitbucket.org/ptsurbeleu/jekyll-lanyon.git', remote.url
  end

  test '.get can set scm remote details' do
    remote = @config.scm.remotes.first

    remote.url = 'https://noop.io/lanyon.git'
    assert_equal 'https://noop.io/lanyon.git', remote.url
  end

  test '.get can set scm branch details' do
    branch = @config.scm.branches.first

    branch.name = 'master'
    assert_equal 'master', branch.name
  end

  test 'can remove script builder by id' do
    refute_nil @config.get_script('jekyll.config.setsetting')
    @config.remove_script('jekyll.config.setsetting')
    assert_nil @config.get_script('jekyll.config.setsetting')
  end

  test 'can gracefully remove script builder by id that does not exist' do
    assert_nil @config.get_script('xoxo')
    @config.remove_script('xoxo')
  end

  test 'can remove parameter by name' do
    refute_nil @config.get_param('BRANCH_SPECIFIER')
    @config.remove_param('BRANCH_SPECIFIER')
    assert_nil @config.get_param('BRANCH_SPECIFIER')
  end

  test 'can gracefully remove parameter by name that does not exist' do
    assert_nil @config.get_param('XYZ')
    @config.remove_param('XYZ')
  end

  test 'can add predefined build trigger' do
    assert @config.triggers.empty?
    trigger = Jenkins::Trigger.poll_scm_every_minute
    @config.add_trigger(trigger)
    assert_equal trigger, @config.triggers.first
  end

  test 'can get build trigger by type' do
    assert @config.triggers.empty?
    trigger = Jenkins::Trigger.poll_scm_every_minute
    @config.add_trigger(trigger)
    assert @config.get_trigger('hudson.triggers.SCMTrigger')
  end

  test '.save can update configuration changes' do
    # make changes
    @config.description = 'xoxoxo'
    # save changes
    Jenkins::JobConfiguration.save(@folder.name, @job_name, @config)
    # reload
    @actual = Jenkins::JobConfiguration.get(@path)
    # assert outcomes
    assert_equal 'xoxoxo', @actual.description
  end
end