require 'test_helper'

class Jenkins::JobTest < Minitest::Test
  def setup
    @folder = Jenkins::Folder.get('jenkins-api')
  end

  test '#display_name returns display name' do
    # arrange
    job = @folder.jobs.find { |o| o.name == 'job-display-name' }
    # assert
    assert_equal 'my custom display name', job.display_name
  end

  test '#name returns name' do
    # arrange
    job = @folder.jobs.find { |o| o.name == 'job-display-name' }
    # assert
    assert_equal 'job-display-name', job.name
  end

  test '#description returns description' do
    # arrange
    job = @folder.jobs.find { |o| o.name == 'job-description' }
    # assert
    assert_equal 'Job description goes here...', job.description
  end
end