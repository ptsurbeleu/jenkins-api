$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)
require 'yaml'
require 'pry'
require 'jenkins'

require 'minitest/autorun'
require 'pry-rescue/minitest'

class Minitest::Test
  def before_setup
    super
    # clear all stubs
    Excon.stubs.clear
  end
end

# http://api.rubyonrails.org/classes/ActiveSupport/Testing/Declarative.html#method-i-test
# Helper to define a test method using a String. Under the hood, it replaces
# spaces with underscores and defines the test method.
#
#   test "verify something" do
#     ...
#   end
def test(name, &block)
    test_name = "test_#{name.gsub(/\s+/,'_')}".to_sym
    defined = method_defined? test_name
    raise "#{test_name} is already defined in #{self}" if defined
    if block_given?
    define_method(test_name, &block)
    else
    define_method(test_name) do
        flunk "No implementation provided for #{name}"
    end
    end
end

def fixtures(bundle, name)
  # Skip mocks if live mode is enabled
  return if ENV['EXCON_LIVE']
  # Enable mocking mode in Excon
  Excon.defaults[:mock] = true
  # load fixture
  yaml = YAML.load_file("./test/fixtures/#{bundle}.yml")
  request, response, bucket = {}, {}, yaml.delete(name.to_s)
  # raise exception when fixture is not found
  raise "Fixture '#{name}' is not found in '#{bundle}.yml'..." unless bucket
  # build request
  %w{method path headers}.each do |p|
    value = bucket.delete(p)
    request[p.to_sym] = value if value
  end
  # build response
  %w{body status}.each do |p|
    value = bucket.delete(p)
    response[p.to_sym] = value if value
  end
  # setup new stub
  Excon.stub(request, response)
end