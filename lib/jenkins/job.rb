# This class represents a Jenkins job
class Jenkins::Job
  attr_accessor :connection, :info, :path
  attr_reader :name, :display_name, :config, :description

  def initialize(connection, hash = {})
   @connection, @name, @description, @info = connection, hash['name'], hash['description'], hash
   @display_name = @info['displayName']
   # parse job path
   @path = @info.delete('url').sub(Jenkins.url, '').chomp('/')
  end

  # Gets a copy path string, used when one job is copied from the other.
  def to_copy_path
    parts = @path.split('/').select {|s| s != 'job' and s != ''}
    parts.join('/')
  end

  def get_config()
    # load current configuration
    config_xml = @connection.get(nil, nil, :path => "#{@path}/config.xml")
    # parse it as xml
    @config = Jenkins::JobConfiguration.new(config_xml)
    # return configuration details back
    @config
  end

  # Set job configuration settings
  def set_properties(opts = {})
    # load configuration
    job_config = @config || get_config
    # try to set properties
    job_config.parameters.each do |p|
      p.value = opts[p.name] if opts.key?(p.name)
    end
    # fetch crumb first
    crumb = @connection.crumb
    crumb['Content-Type'] = 'application/xml;charset=UTF-8'
    # send request
    @connection.post(nil, nil, {
      :headers => crumb,
      :path => "#{@path}/config.xml",
      :body => job_config.to_xml
    })
  end

  # Get all the standard details about the job
  def details
    # load more details
    json = @connection.get("#{@path}")
    job_info = Jenkins::Util.parse_json(json)
  end

  # Last successful build of the job.
  def last_successful_build()
    # load the last successful build details from Jenkins
    json = @connection.get("#{@path}/lastSuccessfulBuild")
    build_info = Jenkins::Util.parse_json(json)
  end

  # Build the job with the specified parameters.
  def build_with_parameters(params = {})
    # fetch crumb first
    crumb, config, query = @connection.crumb, @config || get_config, { }
    crumb['Content-Type'] = 'application/x-www-form-urlencoded'
    # set quiet period on the job
    query[:delay] = "#{config.quiet_period}sec" if config.quiet_period
    # send build request
    @connection.post("#{@path}/buildWithParameters", query, {
      :headers => crumb,
      :body => URI.encode_www_form(params)
    })
  end

  def build
    # fetch crumb first
    crumb, config, query = @connection.crumb, @config || get_config, { }
    # set quiet period on the job
    query[:delay] = "#{config.quiet_period}sec" if config.quiet_period
    # send build request
    @connection.post("#{@path}/build", query, {
      :headers => crumb
    })
  end

  def all_files_in_zip
    # fetch crumb first
    crumb = @connection.crumb
    # send build request
    @connection.get(nil, nil, {
      :headers => crumb,
      :path => "#{@path}/ws/*zip*/#{@name}.zip"
    })
  end

  class << self
    def all(conn = Jenkins.connection)
      hashes = Jenkins::Util.parse_json(conn.get('/view/All'))
      # purge unused portion of the data on the client
      hashes['jobs'].keep_if { |job| job.freestyle_project? }
      # convert from json into an object
      hashes['jobs'].map { |job| new(conn, job) }
    end

    def delete(path, conn = Jenkins.connection)
      # fetch crumb first
      crumb = conn.crumb
      # delete job by posting the specified url
      conn.post(nil, nil, {
        :headers => crumb,
        :path => "#{path}/doDelete",
        :expects => [302, 404]
      })
    end
  end
end

# extensions
class Hash
  def freestyle_project?()
    self['_class'] == 'hudson.model.FreeStyleProject'
  end
end
