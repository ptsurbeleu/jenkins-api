# This is the place for some shared helper functions.
module Jenkins::Util

  module_function

  def parse_json(json)
    JSON.parse(json) unless json.nil?
  end
end
