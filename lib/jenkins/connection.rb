# Connection class
class Jenkins::Connection
  attr_reader :url

  # Creates a new Connection. This method takes a url (String). 
  # This url is passed to Excon, so any options valid for `Excon.new`
  # can be passed here.
  def initialize(url)
    @url = url
  end
  
  def resource
    Excon.new(@url)
  end
  private :resource

  def request(*args, &block)
    params = compile_request_params(*args, &block)
    resource.request(params).body
  end

  def get(*args, &block)
    request(:get, *args, &block)
  end

  def post(*args, &block)
    request(:post, *args, &block)
  end

  def crumb
    # crumb filter needs to be happy
    crumb = Jenkins::Util.parse_json(get('/crumbIssuer'))
    # crumb hash - ready to be merged with headers
    { "#{crumb['crumbRequestField']}" => crumb['crumb'] }
  end

private
  def compile_request_params(http_method, path, query = nil, opts = nil, &block)
    query ||= {}
    opts ||= {}
    headers = opts.delete(:headers) || {}
    content_type = 'application/json'
    user_agent = "Noop/Jenkins-API #{Jenkins::VERSION}"
    authorization = "Basic #{Jenkins::authorization}"
    {
      :method => http_method,
      :path   => "#{path}/api/json",
      :query  => query,
      :headers => {
        'Content-Type' => content_type,
        'User-Agent' => user_agent,
        'Authorization' => authorization,
      }.merge(headers),
      :expects => (200..204).to_a << 304,
      :idempotent => http_method == :get,
      :request_block => block,
    }.merge(opts).reject { |_, v| v.nil? }
  end
end
