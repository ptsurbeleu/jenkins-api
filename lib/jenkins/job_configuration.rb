class Jenkins::JobConfiguration
    attr_reader :parameters, :scm, :builders, :triggers

    def initialize(config_xml)
        @config = Ox.load(config_xml)
        @project = @config.project
        @parameters = parse_parameters
        @scm = parse_source_control_management
        @builders = parse_builders
        @triggers = parse_triggers
    end

    def quiet_period
        @project.quietPeriod.text if @project.respond_to?('quietPeriod')
    end

    def display_name
        @project.displayName.text if @project.respond_to?('displayName')
    end

    def display_name=(value)
        # Special case to nullify the value of the node
        if value.nil?
          @project.nodes.delete(@project.displayName)
          return
        end
        @project.displayName.replace_text(value)
    end

    def description
        @project.description.text
    end

    def description=(value)
        @project.description.replace_text(value)
    end

    def remove_script(id)
        script = self.get_script(id)
        # Gracefully exit if script id is wrong
        return unless script
        @builder_definitions.nodes.delete(script.node)
        @builders.scripts.delete(script)
    end

    def add_trigger(trigger)
        @trigger_definitions << trigger.node
        @triggers << trigger
    end

    def remove_param(name)
        param = self.get_param(name)
        # Gracefully exit if parameter name is wrong
        return unless param
        # Handle removal of the parameter found
        @parameter_definitions.nodes.delete(param.node)
        @parameters.delete(param)
    end

    def get_param(name)
        @parameters.find { |x| x.name == name }
    end

    def get_script(id)
        @builders.scripts.find { |x| x.id == id }
    end

    def get_trigger(type)
        @triggers.find { |x| x.type == type }
    end

    def to_xml
      Ox.dump(@config)
    end

    class << self
      def get(path, conn = Jenkins.connection)
        # load current configuration
        config_xml = conn.get(nil, nil, :path => "#{path}/config.xml")
        # parse it as xml
        Jenkins::JobConfiguration.new(config_xml)
      end

      def save(folder, job, config, conn = Jenkins.connection)
        # fetch crumb first
        crumb = conn.crumb
        crumb['Content-Type'] = 'application/xml;charset=UTF-8'
        # send request
        conn.post(nil, nil, {
            :headers => crumb,
            :path => "/job/#{folder}/job/#{job}/config.xml",
            :body => config.to_xml
        })
      end
    end

    private
    def parse_triggers
        @trigger_definitions = @project.triggers
        @trigger_definitions.nodes.collect do |n|
            Jenkins::Trigger.new(n)
        end
    end

    def parse_builders
        @builder_definitions = @project.builders
        Jenkins::Builders.new(@builder_definitions)
    end

    def parse_source_control_management
        Jenkins::Scm.new(@config.locate('*/scm').first)
    end

    def parse_parameters
        # avoid parsing parameters when none is defined
        return unless @project.properties.respond_to?('hudson.model.ParametersDefinitionProperty')
        container = @project.properties.send('hudson.model.ParametersDefinitionProperty')
        @parameter_definitions = container.parameterDefinitions
        @parameter_definitions.nodes.collect do |n|
            case n.name
              when 'hudson.model.StringParameterDefinition'
                Jenkins::StringParameterDefinition.new(n)
              else
                Jenkins::ParameterDefinition.new(n)
            end
        end
    end
end

class Jenkins::Trigger
    attr_reader :node, :type
 
    def initialize(node)
        @node, @type = node, node.name
    end

    class << self
        def poll_scm_every_minute
            # parse trigger xml
            scm_trigger = Ox.parse('<hudson.triggers.SCMTrigger/>')
            scm_trigger << Ox.parse('<spec>* * * * *</spec>')
            scm_trigger << Ox.parse('<ignorePostCommitHooks>true</ignorePostCommitHooks>')
            # return wrapper object back
            Jenkins::Trigger.new(scm_trigger)
        end
    end
end

class Jenkins::Builders
    attr_reader :scripts, :node

    def initialize(node)
        @node, @scripts = node, parse_scripts(node)
    end

    private
    def parse_scripts(node)
        tag_name = 'org.jenkinsci.plugins.managedscripts.ScriptBuildStep'
        node.locate(tag_name).collect do |n|
            Jenkins::ScriptBuildStep.new(n)
        end
    end
end

class Jenkins::ScriptBuildStep
    attr_reader :node

    def initialize(node)
        @node = node
    end

    def id
        @node.buildStepId.text
    end
end

class Jenkins::Scm
    attr_reader :remotes, :branches

    def initialize(node)
        @node, @remotes, @branches = node, parse_remotes(node), parse_branches(node)
    end

    private
    def parse_branches(node)
        node.locate('branches/*').collect do |n|
            Jenkins::Branch.new(n)
        end
    end

    def parse_remotes(node)
        node.locate('userRemoteConfigs/*').collect do |n|
            Jenkins::Remote.new(n)
        end
    end
end

class Jenkins::Branch
    def initialize(node)
        @node = node
    end

    def name
        @node.locate('name').first.text
    end

    def name=(value)
        @node.locate('name').first.replace_text(value)
    end
end

class Jenkins::Remote
    def initialize(node)
        @node = node
    end

    def url
        @node.locate('url').first.text
    end

    def url=(value)
        @node.locate('url').first.replace_text(value)
    end
end

class Jenkins::ParameterDefinition
    attr_reader :type_name, :name, :node

    def initialize(node)
        @node, @type_name = node, node.name
        # build index from inner nodes
        nodes = node.nodes.collect { |n| [n.name.to_sym, n] }
        @index = Hash[*nodes.flatten]
        # set name of the parameter
        @name = @index[:name].text
    end
end

class Jenkins::StringParameterDefinition < Jenkins::ParameterDefinition
    def value
        @index[:defaultValue].text
    end

    def value=(value)
        @index[:defaultValue].replace_text(value)
    end
end