# This class represents a Jenkins folder (organizing jobs)
class Jenkins::Folder
  attr_accessor :info, :jobs, :connection
  attr_reader :name

  def initialize(conn, hash = {})
    @name, @info, @connection = hash['name'], hash, conn
    @jobs = @info['jobs'].map { |hash| Jenkins::Job.new(@connection, hash) } if @info['jobs']
    # parse folder path
    @path = @info.delete('url').sub(Jenkins.url, '').chomp('/')
  end

  def create_copy_from(name, from)
    # fetch crumb first
    crumb = @connection.crumb
    # setup query string
    query = { 'name' => name, 'mode' => 'copy', 'from' => from }
    # send build request
    @connection.post(nil, query, {
      :headers => crumb,
      :path => "#{@path}/createItem",
      :expects => 302
    })
  end

  def get_job(name)
    # parse job info from json
    hash = Jenkins::Util.parse_json(@connection.get("#{@path}/job/#{name}"))
    Jenkins::Job.new(@connection, hash)
  end

  def activate_job(name)
    # fetch crumb first
    crumb = @connection.crumb
    crumb['Content-Type'] = 'application/x-www-form-urlencoded'
    # activate job by posting an empty description
    @connection.post(nil, nil, {
      :headers => crumb,
      :path => "#{@path}/job/#{name}/description",
      :body => URI.encode_www_form({ :description => '' }),
      :expects => 204
    })
  end

  class << self
    def get(id, conn = Jenkins.connection)
      begin
        # might encounter 404
        hash = Jenkins::Util.parse_json(conn.get("/job/#{id}", 'tree=name,url,jobs[name,displayName,url,color,description]'))
        # folder or nil if empty
        hash.folder? ? new(conn, hash) : nil
      rescue Excon::Error::NotFound => e
        nil
      end
    end

    def delete(name, conn = Jenkins.connection)
      # fetch crumb first
      crumb = conn.crumb
      # delete job by posting the specified url
      conn.post(nil, nil, {
        :headers => crumb,
        :path => "/job/#{name}/doDelete",
        :expects => [302, 404]
      })
    end

    def create(name, conn = Jenkins.connection)
      # crumb filter needs to be happy
      crumb = Jenkins::Util.parse_json(conn.get('/crumbIssuer'))
      # only xml is allowed to create items..
      headers = { 'Content-Type' => 'application/xml' }
      # inject crub to unblock the request
      headers[crumb['crumbRequestField']] = crumb['crumb']
      # setup query string
      query = { 'name' => name }
      # send the request
      conn.post('/createItem', query, {
        :headers => headers,
        :body => '<com.cloudbees.hudson.plugins.folder.Folder/>'
      })
    end

    def all(conn = Jenkins.connection)
      hashes = Jenkins::Util.parse_json(conn.get('view/All'))
      # purge unused portion of the data on the client
      hashes['jobs'].keep_if { |o| o.folder? }
      # convert from json into an object
      hashes['jobs'].map { |hash| new(conn, hash) }
    end
  end
end

class Hash
  def folder?()
    self['_class'] == 'com.cloudbees.hudson.plugins.folder.Folder'
  end
end
