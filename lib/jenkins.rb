require "excon"
require "base64"
require "json"
require "ox"

module Jenkins
  require "jenkins/version"
  require "jenkins/connection"
  require "jenkins/job"
  require "jenkins/util"
  require "jenkins/folder"
  require "jenkins/job_configuration"

  def url
    @url ||= ENV['JENKINS_URL']
  end

  def api_token
    @secret ||= ENV['JENKINS_API_TOKEN']
  end

  def api_login
    @login ||= ENV['JENKINS_API_LOGIN']
  end

  def authorization
    Base64.strict_encode64 "#{api_login}:#{api_token}"
  end

  def connection
    @connection ||= Connection.new(url)
  end

  module_function :url, :api_token, :api_login, :authorization, :connection
end
